using System.Threading.Tasks;
using ApiService.Infrastructure.Dto;
using ApiService.Services;
using CSharpFunctionalExtensions;
using Microsoft.AspNetCore.Mvc;

namespace ApiService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly UserService _userService;

        public AuthController(UserService userService)
        {
            _userService = userService;
        }

        [HttpPost("requestSmsCode")]
        public async Task<IActionResult> RequestSmsCode(RequestSmsCodeDto dto)
        {
            Result result = await _userService.RequestSmsCode(dto);
            return result.IsSuccess ? Ok() : BadRequest(new { result.Error });
        }
    }
}
