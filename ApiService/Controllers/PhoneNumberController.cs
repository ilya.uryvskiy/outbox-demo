using System;
using System.Threading.Tasks;
using ApiService.Infrastructure.Dto;
using ApiService.Services;
using Microsoft.AspNetCore.Mvc;

namespace ApiService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PhoneNumberController : ControllerBase
    {
        private readonly PhoneNumberService _phoneNumberService;

        public PhoneNumberController(PhoneNumberService phoneNumberService)
        {
            _phoneNumberService = phoneNumberService;
        }

        [HttpPost]
        public async Task<IActionResult> Create(PhoneNumberDto dto)
        {
            var id = await _phoneNumberService.Create(dto);
            return Ok(id);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, PhoneNumberDto dto)
        {
            await _phoneNumberService.Update(id, dto);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _phoneNumberService.Delete(id);
            return Ok();
        }
    }
}
