using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using ApiService.DataAccessLayer.Models;
using MessageContracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace ApiService.DataAccessLayer
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        public DbSet<ConfirmationCode> ConfirmationCodes { get; set; }

        public DbSet<PhoneNumber> PhoneNumbers { get; set; }

        public DbSet<Outbox> Outboxes { get; set; }

        public async Task PublishMessage<T>(T message)
            where T : class, new()
        {
            var outbox = new Outbox(message);
            await Outboxes.AddAsync(outbox);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            EmitTrackingEvents().Wait();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            await EmitTrackingEvents();
            var saved = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            return saved;
        }

        private async Task EmitTrackingEvents()
        {
            var entries = ChangeTracker.Entries()
                .Where(x => x.Entity.GetType() != typeof(Outbox))
                .ToList();
            foreach (var entry in entries)
            {
                var trackingEvent = new TrackingEvent();
                switch (entry.State)
                {
                    case EntityState.Added:
                        trackingEvent.Type = TrackingEventType.Created;
                        break;
                    case EntityState.Deleted:
                        trackingEvent.Type = TrackingEventType.Deleted;
                        break;
                    case EntityState.Modified:
                        trackingEvent.Type = TrackingEventType.Modified;
                        trackingEvent.Changes.AddRange(FindEntityDiff(entry));
                        break;
                    case EntityState.Unchanged:
                    case EntityState.Detached:
                        continue;
                    default:
                        throw new ArgumentOutOfRangeException();

                }
                trackingEvent.EntityName = entry.CurrentValues.EntityType.Name;
                trackingEvent.Entity = JsonSerializer.Serialize(entry.Entity);
                await PublishMessage(trackingEvent);
            }
        }

        private List<EntityPropertyChange> FindEntityDiff(EntityEntry entry)
        {
            var result = new List<EntityPropertyChange>();
            foreach (var originalProperty in entry.OriginalValues.Properties)
            {
                var currentProperty = entry.CurrentValues.Properties
                    .FirstOrDefault(x => x.Name == originalProperty.Name);
                if (currentProperty != null)
                {
                    var currentValue = entry.CurrentValues[currentProperty.Name];
                    var originalValue = entry.OriginalValues[originalProperty.Name];

                    if (currentValue == null && originalValue == null)
                    {
                        continue;
                    }

                    if (currentValue !=null && originalValue != null && currentValue.Equals(originalValue))
                    {
                        continue;
                    }

                    var entityPropertyChange = new EntityPropertyChange
                    {
                        PropertyName = currentProperty.Name,
                        OriginalValue = originalValue?.ToString(),
                        CurrentValue = currentValue?.ToString()
                    };

                    result.Add(entityPropertyChange);
                }
            }

            return result;
        }
    }
}
