using System;

namespace ApiService.DataAccessLayer.Models
{
    public class ConfirmationCode
    {
        public ConfirmationCode(string phone, string code)
        {
            Id = Guid.NewGuid();
            Phone = phone;
            Code = code;
        }

        public ConfirmationCode()
        {
        }

        public Guid Id { get; private set; }

        public string Phone { get; private set; }

        public string Code { get; private set; }
    }
}
