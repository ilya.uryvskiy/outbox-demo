using System;
using System.Text.Json;

namespace ApiService.DataAccessLayer.Models
{
    public class Outbox
    {
        public Outbox(object message)
        {
            Id = Guid.NewGuid();
            EntityType = message.GetType().AssemblyQualifiedName;
            Payload = JsonSerializer.Serialize(message);
            IsSent = false;
            AddedAt = DateTime.UtcNow;
        }

        public Outbox()
        {
        }

        public Guid Id { get; private set; }

        public string EntityType { get; private set; }

        public string Payload { get; private set; }

        public bool IsSent { get; private set; }

        public DateTime AddedAt { get; private set; }

        public void MarkAsSent()
        {
            IsSent = true;
        }
    }
}
