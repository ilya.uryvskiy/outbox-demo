using System;

namespace ApiService.DataAccessLayer.Models
{
    public class PhoneNumber
    {
        public PhoneNumber(string name, string phone)
        {
            Id = Guid.NewGuid();
            Name = name;
            Phone = phone;

        }

        public PhoneNumber()
        {
        }

        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public string Phone { get; private set; }

        public void SetName(string name)
        {
            Name = name;
        }

        public void SetPhone(string phone)
        {
            Phone = phone;
        }
    }
}
