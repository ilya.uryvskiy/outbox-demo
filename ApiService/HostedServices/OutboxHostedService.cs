using System;
using System.Threading;
using System.Threading.Tasks;
using ApiService.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ApiService.HostedServices
{
    public class OutboxHostedService : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        private Task _task;

        public OutboxHostedService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _task = Task.Run(SendOutbox);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            return Task.CompletedTask;
        }

        private async Task SendOutbox()
        {
            while (true)
            {
                try
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var outboxService = scope.ServiceProvider.GetRequiredService<OutboxService>();
                        await outboxService.ProcessOutbox();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    await Task.Delay(300);
                }

            }
        }
    }
}
