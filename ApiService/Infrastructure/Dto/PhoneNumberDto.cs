namespace ApiService.Infrastructure.Dto
{
    public class PhoneNumberDto
    {
        public string Name { get; set; }

        public string Phone { get; set; }
    }
}
