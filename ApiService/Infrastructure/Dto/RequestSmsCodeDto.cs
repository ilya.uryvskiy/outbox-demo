namespace ApiService.Infrastructure.Dto
{
    public class RequestSmsCodeDto
    {
        public string Phone { get; set; }
    }
}
