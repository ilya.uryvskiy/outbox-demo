namespace ApiService.Infrastructure.Settings
{
    public class TransitSettings
    {
        public string Host { get; set; }

        public string Vhost { get; set; }

        public ushort Port { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
