using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DataAccessLayer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ApiService
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // https://microservices.io/patterns/data/transactional-outbox.html
            try
            {
                var host = CreateHostBuilder(args).Build();
                var provider = host.Services;

                using (var scope = provider.CreateScope())
                {
                    await InitializeDataBase(scope.ServiceProvider);
                }

                await host.RunAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Host terminated: {e}");
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });

        private static async Task InitializeDataBase(IServiceProvider services)
        {
            var logger = services.GetRequiredService<ILogger<Program>>();

            try
            {
                logger.LogInformation("Database initialization");

                logger.LogInformation($"Applying {nameof(ApiContext)} migrations");
                var phcContext = services.GetRequiredService<ApiContext>();
                await phcContext.Database.MigrateAsync();
                logger.LogInformation("Database initialization completed successfully");
            }
            catch (Exception ex)
            {
                logger.LogError($"Database migration error.\n{ex.Message}\n{ex}");
                throw;
            }
        }
    }
}
