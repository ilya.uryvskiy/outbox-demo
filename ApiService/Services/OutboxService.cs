using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using ApiService.DataAccessLayer;
using MassTransit;
using MessageContracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ApiService.Services
{
    public class OutboxService
    {
        private const int BatchSize = 5;

        private readonly ApiContext _apiContext;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly ILogger<OutboxService> _logger;

        public OutboxService(
            ApiContext apiContext,
            IPublishEndpoint publishEndpoint,
            ILogger<OutboxService> logger)
        {
            _apiContext = apiContext;
            _publishEndpoint = publishEndpoint;
            _logger = logger;
        }

        public async Task ProcessOutbox()
        {
            // _logger.LogInformation("ProcessOutbox");
            using (var transaction = await _apiContext.Database.BeginTransactionAsync())
            {
                try
                {
                    var outboxes = await _apiContext.Outboxes
                        .Where(x => !x.IsSent)
                        .OrderByDescending(x => x.AddedAt)
                        .Take(BatchSize).ToListAsync();
                    foreach (var outbox in outboxes)
                    {
                        var typeName = outbox.EntityType;
                        var type = Type.GetType(typeName);
                        if (type == null)
                        {
                            continue;
                        }

                        var payload = outbox.Payload;
                        var message = JsonSerializer.Deserialize(payload, type);
                        if (message is IMessage)
                        {
                            (message as IMessage).PublishId = outbox.Id;
                        }

                        await _publishEndpoint.Publish(message);
                        outbox.MarkAsSent();
                    }

                    await _apiContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (Exception e)
                {
                    _logger.LogError("Error", e);
                    await transaction.RollbackAsync();
                }
            }
        }
    }
}
