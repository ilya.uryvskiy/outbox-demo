using System;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DataAccessLayer;
using ApiService.DataAccessLayer.Models;
using ApiService.Infrastructure.Dto;
using Microsoft.EntityFrameworkCore;

namespace ApiService.Services
{
    public class PhoneNumberService
    {
        private readonly ApiContext _apiContext;

        public PhoneNumberService(ApiContext apiContext)
        {
            _apiContext = apiContext;
        }

        public async Task<Guid> Create(PhoneNumberDto dto)
        {
            using (var transaction = await _apiContext.Database.BeginTransactionAsync())
            {
                var newPhoneNumber = new PhoneNumber(dto.Name, dto.Phone);
                await _apiContext.PhoneNumbers.AddAsync(newPhoneNumber);

                await _apiContext.SaveChangesAsync();
                await transaction.CommitAsync();

                return newPhoneNumber.Id;
            }
        }

        public async Task Update(Guid id, PhoneNumberDto dto)
        {
            using (var transaction = await _apiContext.Database.BeginTransactionAsync())
            {
                var phoneNumber = await _apiContext.PhoneNumbers.FirstOrDefaultAsync(x => x.Id == id);
                phoneNumber.SetName(dto.Name);
                phoneNumber.SetPhone(dto.Phone);

                await _apiContext.SaveChangesAsync();
                await transaction.CommitAsync();
            }
        }

        public async Task Delete(Guid id)
        {
            using (var transaction = await _apiContext.Database.BeginTransactionAsync())
            {
                var phoneNumber = await _apiContext.PhoneNumbers.FirstOrDefaultAsync(x => x.Id == id);
                _apiContext.PhoneNumbers.Remove(phoneNumber);

                await _apiContext.SaveChangesAsync();
                await transaction.CommitAsync();
            }
        }
    }
}
