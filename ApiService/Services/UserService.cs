using System;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DataAccessLayer;
using ApiService.DataAccessLayer.Models;
using ApiService.Infrastructure.Dto;
using CSharpFunctionalExtensions;
using MessageContracts;

namespace ApiService.Services
{
    public class UserService
    {
        private readonly ApiContext _context;

        public UserService(ApiContext context)
        {
            _context = context;
        }

        public async Task<Result> RequestSmsCode(RequestSmsCodeDto dto)
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var smsCode = GenerateSmsCode(4);
                    var confirmationCode = new ConfirmationCode(dto.Phone, smsCode);
                    await _context.ConfirmationCodes.AddAsync(confirmationCode);

                    var sendSmsMessage = new SendSms
                    {
                        Phone = dto.Phone,
                        Text = $"Your code is {smsCode}"
                    };
                    await _context.PublishMessage(sendSmsMessage);

                    await _context.SaveChangesAsync();
                    await transaction.CommitAsync();
                    return Result.Success();
                }
                catch (Exception e)
                {
                    await transaction.RollbackAsync();
                    return Result.Failure("Error occured");
                }
            }
        }

        private string GenerateSmsCode(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var result = new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }
    }
}
