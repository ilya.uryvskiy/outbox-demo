using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiService.DataAccessLayer;
using ApiService.HostedServices;
using ApiService.Infrastructure.Settings;
using ApiService.Services;
using MassTransit;
using MassTransit.RabbitMqTransport;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ApiService
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddTransient<UserService>();
            services.AddTransient<OutboxService>();
            services.AddTransient<PhoneNumberService>();

            var connectionString = _configuration.GetConnectionString("DatabaseConnection");
            services.AddDbContext<ApiContext>(options => options.UseNpgsql(connectionString));

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, config) =>
                {
                    var configSection = _configuration.GetSection("TransitSettings");
                    var transitSettings = configSection.Get<TransitSettings>();
                    config.Host(transitSettings.Host, transitSettings.Port, transitSettings.Vhost, c =>
                    {
                        c.Username(transitSettings.Username);
                        c.Password(transitSettings.Password);
                    });
                });
            });

            services.AddMassTransitHostedService();
            services.AddHostedService<OutboxHostedService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
