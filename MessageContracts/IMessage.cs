using System;

namespace MessageContracts
{
    public interface IMessage
    {
        public Guid PublishId { get; set; }
    }
}
