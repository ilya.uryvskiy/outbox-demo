﻿using System;

namespace MessageContracts
{
    public class SendSms : IMessage
    {
        public Guid PublishId { get; set; }

        public string Phone { get; set; }

        public string Text { get; set; }
    }
}
