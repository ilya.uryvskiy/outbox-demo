using System;
using System.Collections.Generic;

namespace MessageContracts
{
    public class TrackingEvent : IMessage
    {
        public TrackingEvent()
        {
            Changes = new List<EntityPropertyChange>();
        }

        public string EntityName { get; set; }

        public TrackingEventType Type { get; set; }

        public List<EntityPropertyChange> Changes { get; set; }

        public string Entity { get; set; }
        public Guid PublishId { get; set; }
    }

    public enum TrackingEventType
    {
        Created = 0,
        Modified = 1,
        Deleted = 2
    }

    public class EntityPropertyChange
    {
        public string PropertyName { get; set; }

        public string OriginalValue { get; set; }

        public string CurrentValue { get; set; }
    }
}
