using System.Threading.Tasks;
using MassTransit;
using MessageContracts;
using Microsoft.Extensions.Logging;

namespace SmsService.Consumers
{
    public class SendSmsConsumer : IConsumer<SendSms>
    {
        private readonly ILogger<SendSmsConsumer> _logger;

        public SendSmsConsumer(ILogger<SendSmsConsumer> logger)
        {
            _logger = logger;
        }

        public Task Consume(ConsumeContext<SendSms> context)
        {
            var sendSms = context.Message;
            _logger.LogInformation($"Sending SMS to number '{sendSms.Phone}' with message '{sendSms.Text}'");
            return Task.CompletedTask;
        }
    }
}
