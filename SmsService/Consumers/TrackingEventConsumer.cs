using System;
using System.Threading.Tasks;
using MassTransit;
using MessageContracts;
using Microsoft.Extensions.Logging;

namespace SmsService.Consumers
{
    public class TrackingEventConsumer : IConsumer<TrackingEvent>
    {
        private readonly ILogger<TrackingEventConsumer> _logger;

        public TrackingEventConsumer(ILogger<TrackingEventConsumer> logger)
        {
            _logger = logger;
        }

        public Task Consume(ConsumeContext<TrackingEvent> context)
        {
            var message = context.Message;
            var logMessage = $"Entity with type {message.EntityName} was {message.Type}"
                             + Environment.NewLine
                             + $"Entity: '{message.Entity}'";
            if (message.Type == TrackingEventType.Modified)
            {
                logMessage += Environment.NewLine + "The following properties ware changed:";
                foreach (var change in message.Changes)
                {
                    logMessage += Environment.NewLine + $"{change.PropertyName}: '{change.OriginalValue}' => '{change.CurrentValue}'";
                }
            }

            _logger.LogInformation(logMessage);
            return Task.CompletedTask;
        }
    }
}
