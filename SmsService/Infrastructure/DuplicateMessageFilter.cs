using System;
using System.Threading.Tasks;
using GreenPipes;
using MassTransit;
using MessageContracts;

namespace SmsService.Infrastructure
{
    public class DuplicateMessageFilter<T> :  IFilter<ConsumeContext<T>>
        where T : class
    {
        public async Task Send(ConsumeContext<T> context, IPipe<ConsumeContext<T>> next)
        {
            var message = context.Message;
            if (message is IMessage iMessage)
            {
                Console.WriteLine(iMessage.PublishId);
            }
            await next.Send(context);
        }

        public void Probe(ProbeContext context)
        {
        }
    }
}
