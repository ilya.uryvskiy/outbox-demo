using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SmsService.Consumers;
using SmsService.Infrastructure;
using SmsService.Infrastructure.Settings;

namespace SmsService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;
                    services.AddMassTransit(x =>
                    {
                        x.AddConsumer<SendSmsConsumer>();
                        x.AddConsumer<TrackingEventConsumer>();
                        x.UsingRabbitMq((context, config) =>
                        {
                            var configSection = configuration.GetSection("TransitSettings");
                            var transitSettings = configSection.Get<TransitSettings>();
                            config.Host(transitSettings.Host, transitSettings.Port, transitSettings.Vhost, c =>
                            {
                                c.Username(transitSettings.Username);
                                c.Password(transitSettings.Password);
                            });

                            config.ReceiveEndpoint("send-sms", e =>
                            {
                                e.UseConsumeFilter(typeof(DuplicateMessageFilter<>), context);
                                e.ConfigureConsumer<SendSmsConsumer>(context);
                            });

                            config.ReceiveEndpoint("tracking-event", e =>
                            {
                                e.ConfigureConsumer<TrackingEventConsumer>(context);
                            });
                        });
                    });

                    services.AddMassTransitHostedService();
                });
    }
}
